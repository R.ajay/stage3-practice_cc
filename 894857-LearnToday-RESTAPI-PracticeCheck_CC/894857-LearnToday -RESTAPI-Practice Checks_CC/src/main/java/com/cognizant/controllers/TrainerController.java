package com.cognizant.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.exceptions.ErrorDetails;
import com.cognizant.exceptions.ResourceAlreadyExists;
import com.cognizant.exceptions.ResourceNotFoundException;
import com.cognizant.models.Trainer;
import com.cognizant.services.TrainerServices;

@RestController
@RequestMapping(value="/api")
public class TrainerController {
		@Autowired
		private TrainerServices trainerservice;
		@PostMapping(value="/trainer")
		public ResponseEntity<?> trainerSignup(@RequestBody Trainer trainer){
			Trainer tr=trainerservice.find(trainer.getTrainerId());
			
			try {
				if(tr==null) {
					trainerservice.insertTrainer(trainer);
					return new ResponseEntity<Trainer>(trainer,HttpStatus.CREATED);
				}
				else {
					ResourceAlreadyExists rnfe = new ResourceAlreadyExists("Trainer Already Exists");
					System.out.println(rnfe.getMessage());
					ErrorDetails errors = new ErrorDetails(rnfe.getMessage());
					return new ResponseEntity<Object>(errors, HttpStatus.BAD_REQUEST);

				}
			} catch (Exception e) {
				// TODO: handle exception
				return new ResponseEntity<Exception>(e, HttpStatus.BAD_REQUEST);

			}

		}
		
		@PutMapping(value="/trainer/{TrainerId}")
		public ResponseEntity<?> updatepassword(@PathVariable("TrainerId")int TrainerId,@RequestBody Trainer trainer)
		{
			Trainer tr=new Trainer();
			tr.setTrainerId(TrainerId);
			tr.setPassword(trainer.getPassword());
			Trainer train=trainerservice.find(TrainerId);
			try {
				
			
			if(train!=null) {
				trainerservice.updateTrainer(tr);
				return new ResponseEntity<String>("data updated successfully",HttpStatus.OK);
			}
			else {
				ResourceNotFoundException rnfe = new ResourceNotFoundException("Searched data Not Found");
				System.out.println(rnfe.getMessage());
				ErrorDetails errors = new ErrorDetails(rnfe.getMessage());
				return new ResponseEntity<Object>(errors, HttpStatus.NOT_FOUND);
//				return new ResponseEntity<ResourceNotFoundException>(HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Exception>(e, HttpStatus.BAD_REQUEST);

		}
			
		}
}
