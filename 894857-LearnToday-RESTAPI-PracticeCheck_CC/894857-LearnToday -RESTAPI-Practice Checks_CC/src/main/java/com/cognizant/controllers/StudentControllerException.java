package com.cognizant.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.cognizant.exceptions.BadRequestException;
import com.cognizant.exceptions.ErrorDetails;

@ControllerAdvice
public class StudentControllerException {
	
	@ExceptionHandler(com.cognizant.exceptions.BadRequestException.class)
	public ResponseEntity<Object> handleBadRequestException(BadRequestException ex) {

		ErrorDetails errors = new ErrorDetails(ex.getMessage());

		return new ResponseEntity<Object>(errors, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(com.cognizant.exceptions.ResourceAlreadyExists.class)
	public ResponseEntity<Object> handleResoourceAlreadyExistsException(com.cognizant.exceptions.ResourceAlreadyExists ex) {

		ErrorDetails errors = new ErrorDetails(ex.getMessage());

		return new ResponseEntity<Object>(errors, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(com.cognizant.exceptions.ResourceNotFoundException.class)
	public ResponseEntity<Object> handleResourceNotFoundException(com.cognizant.exceptions.ResourceNotFoundException ex) {

		ErrorDetails errors = new ErrorDetails(ex.getMessage());

		return new ResponseEntity<Object>(errors, HttpStatus.NOT_FOUND);
	}

}
