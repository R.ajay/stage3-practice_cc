package com.cognizant.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.dao.CourseDTO;
import com.cognizant.models.Course;
import com.cognizant.services.CourseServices;

@RestController
@RequestMapping("/api")
public class AdminController {

	@Autowired
	private CourseServices courseservices;

	// Controller for getting all courses
	@GetMapping(value = "/Admin")
	public ResponseEntity<CourseDTO> getAllCourses() {
		CourseDTO coursedto = new CourseDTO();
		coursedto.setCourselist(courseservices.getAllCourses());
		return new ResponseEntity<CourseDTO>(coursedto, HttpStatus.OK);
	}

	// Controller for getting all courses
	@GetMapping(value = "/Admin/{CourseId}")
	public ResponseEntity<?> getCourseById(@PathVariable("CourseId") int CourseId) {
//			coursedto.setCourselist(courseservices.getAllCourses());
		Course course = courseservices.getCourseById(CourseId);
		return new ResponseEntity<Course>(course,HttpStatus.OK);

	}

}
