package com.cognizant.dao;

import java.util.List;

import com.cognizant.models.Course;

public class CourseDTO {
	
	private List<Course> courselist;

	public List<Course> getCourselist() {
		return courselist;
	}

	public void setCourselist(List<Course> courselist) {
		this.courselist = courselist;
	}
	

}
