package com.cognizant.dao;

import org.springframework.stereotype.Component;

import com.cognizant.models.Trainer;

@Component
public interface TrainerDao {
	
	public boolean insert(Trainer trainer);//for registering the trainer
	public boolean update(Trainer trainer);//for updating
	public Trainer find(int TrainerId);//for finding and throwing error

}
