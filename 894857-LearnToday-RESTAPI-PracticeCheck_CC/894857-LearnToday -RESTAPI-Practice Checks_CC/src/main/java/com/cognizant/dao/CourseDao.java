package com.cognizant.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.cognizant.models.Course;
@Component
public interface CourseDao {
	
	public List<Course> getAllCourses();//for getting all course from DB
	public Course getCourseById(int CourseId);//for getting courses by ID from DB
}
