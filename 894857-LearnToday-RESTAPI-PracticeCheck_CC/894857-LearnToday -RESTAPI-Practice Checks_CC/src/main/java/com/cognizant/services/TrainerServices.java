package com.cognizant.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.dao.TrainerDao;
import com.cognizant.models.Trainer;

@Service
public class TrainerServices {
	
	@Autowired
	private TrainerDao trainerdao;
	
	public boolean insertTrainer(Trainer trainer) {
		return trainerdao.insert(trainer);
	}
	
	public boolean updateTrainer(Trainer trainer) {
		return trainerdao.update(trainer);
	}
	
	public Trainer find(int TrainerId) {
		return trainerdao.find(TrainerId);
	}

}
