package com.cognizant.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.dao.StudentDao;
import com.cognizant.models.Course;
import com.cognizant.models.Student;

@Service
public class StudentServices {
	@Autowired
	private StudentDao studentdao;
	
	public List<Course> getAllCourses(){
		return studentdao.getAllCourses();
	}
	
	public Student getStudentById(int EnrollmentId) {
		return studentdao.getStudentById(EnrollmentId);
	}
	
	
	public boolean postStudent(Student student) {
		boolean res=studentdao.insert(student);
		return res;
		
	}
	
	public boolean deleteStudent(int EnrollmentId) {
		boolean res=studentdao.delete(EnrollmentId);
		return res;
	}
	

}
