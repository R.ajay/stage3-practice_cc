package com.cognizant.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.dao.CourseDao;
import com.cognizant.exceptions.ResourceNotFoundException;
import com.cognizant.models.Course;

@Service
public class CourseServices {
	@Autowired
	private CourseDao coursedao;
	
	public List<Course> getAllCourses(){
		return coursedao.getAllCourses();
		
	}
	
	public Course getCourseById(int CourseId) {
//		return coursedao.getCourseById(CourseId);
		Course course=coursedao.getCourseById(CourseId);
		if(course==null) {
			throw new ResourceNotFoundException("Searched Data Not Found");
		}
		else {
			return course;
		}
	}

}
