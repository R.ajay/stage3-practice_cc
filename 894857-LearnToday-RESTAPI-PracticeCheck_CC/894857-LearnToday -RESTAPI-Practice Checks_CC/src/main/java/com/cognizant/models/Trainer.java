package com.cognizant.models;

public class Trainer {
	
	private int TrainerId;
	private String Password;
	public int getTrainerId() {
		return TrainerId;
	}
	public void setTrainerId(int trainerId) {
		TrainerId = trainerId;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public Trainer(int trainerId, String password) {
		super();
		TrainerId = trainerId;
		Password = password;
	}
	public Trainer() {
		super();
	}
	@Override
	public String toString() {
		return "Trainer [TrainerId=" + TrainerId + ", Password=" + Password + "]";
	}
	

}
